import documentReady from "./helpers/documentReady";
import lazyImages from "./modules/lazyImages";
import paymentMagicLine from "./modules/paymentMagicLine";
import openMenu from "./modules/openMenu";
import initSlider from "./modules/initSlider";
import initTabs from "./modules/initTabs";
import initDrop from "./modules/initDrop";
import initGallery from "./modules/initGallery";
import Modal from "./modules/initModal";
import validation from "./modules/validation";
import input from "./modules/input";
import onOpenEmergencyModal from "./modules/onOpenEmergencyModal";
import setPersentageCard from "./modules/setPersentageCard";
import onOpenPetModal from "./modules/onOpenPetModal";
import $ from 'jquery';

window.onload = function () {
	const prealoader = document.querySelector(".preloader");

	setTimeout(() => {
		prealoader.classList.add("prealoader_is-loaded")
	}, 100);

	setTimeout(() => {
		prealoader.style.display = "none";
	}, 300);
}

documentReady(() => {
	lazyImages();
	paymentMagicLine();
	openMenu();
	initSlider();
	initTabs();
	initDrop();
	initGallery();
	validation();
	input();
	onOpenEmergencyModal();
	setPersentageCard();
	onOpenPetModal();


	const paymentCheckbox = document.querySelectorAll(".payment-form__checkbox-label");
	const paymentCheckboxInpts = document.querySelectorAll(".js-price-min");
	const paymentPriceInpts = document.querySelectorAll(".js-price");
	var price;

	paymentPriceInpts.forEach((el) => {
		el.addEventListener("change", (e) => {
			let form = e.target.closest("form");
			let val = el.value;

			console.log(val);

			if (!val) {
				form.querySelectorAll(".js-price-min").forEach((el)=>{
					console.log("123");
					el.setAttribute("data-parsley-required", true);
					el.closest(".payment-form__checkbox-label").classList.remove("_disabled");
				});
			} else {
				if (form.querySelector(".payment-form__checkbox-wrap").querySelector(".parsley-errors-list")) {
					form.querySelector(".payment-form__checkbox-wrap").querySelector(".parsley-errors-list").remove();
				}

				form.querySelectorAll(".js-price-min").forEach((el)=>{
					el.removeAttribute("data-parsley-required");
					el.closest(".payment-form__checkbox-label").classList.add("_disabled");

					if (el.checked == true) {
						el.checked = false;
						el.classList.remove("parsley-error");
					}
				});
			}
		});
	});

	paymentCheckbox.forEach((el) => {
		el.addEventListener("click", (e) => {
			let form = e.target.closest("form");

			for (var i = 0, length = paymentCheckboxInpts.length; i < length; i++) {
				if (paymentCheckboxInpts[i].checked) {
					price = paymentCheckboxInpts[i].value;
					form.querySelector(".js-price").removeAttribute("data-parsley-required");
					form.querySelector(".js-price").closest(".payment-form__input-container").classList.add("_disabled");

					if(form.querySelector(".parsley-errors-list")) {
						form.querySelector(".js-price").classList.remove("parsley-error");
						form.querySelector(".parsley-errors-list").remove();
					}
					break;
				}
			}

		});
	});

});
