import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

export default () => {
	const options = {
		reserveScrollBarGap: true,
	};

	const menu = document.querySelector(".header-nav");
	const menuBtn = document.querySelector(".header__button-burger");
	const header = document.querySelector(".header");
	let flag = false;

	menuBtn.addEventListener("click", (e)=>{
		let self = e.currentTarget;

		switch (flag) {
			case false:
				header.classList.add("menu-is-active");
				self.classList.add("_is-active");
				menu.classList.add("_is-open");
				disableBodyScroll(menu, options);
				flag = true;
				break;
			case true:
				header.classList.remove("menu-is-active");
				self.classList.remove("_is-active");
				menu.classList.remove("_is-open");
				clearAllBodyScrollLocks();
				flag = false;
				break;
		}

	});
}
