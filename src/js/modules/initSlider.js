import { Slider } from "./slider";

// 1ур. .gl-slider.js-swiper data-swiper-slides-preview="4" + data-swiper-gap="20"? - общая обертка:
// 2ур. .gl-slider__container.swiper-container.js-swiper-container
// 3ур. .gl-slider__wrapper.swiper-wrapper
// 4ур. .swiper-slide

export default () => {
	const sliderWrapperElements = Array.from(document.querySelectorAll(`.js-swiper`));
	if (!sliderWrapperElements) return;

	sliderWrapperElements.map(sliderElem => new Slider(
	  sliderElem.querySelector('.js-swiper-container'),
	  sliderElem.getAttribute('data-swiper-slides-preview'),
	  sliderElem.querySelector('.js-slider-btn-prev'),
	  sliderElem.querySelector('.js-slider-btn-next'),
	  sliderElem.querySelector('.js-pagination-slider'),
	  sliderElem.getAttribute('data-swiper-gap')
	));
}
