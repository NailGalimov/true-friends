import $ from 'jquery';

export default () => {
	var input = $(".input");

	if (input.length) {
		input.each(function() {
			var $this = $(this);
			var thisVal = $(this).val();

			if (thisVal.length !== 0) {
				$this.closest(".payment-form__label").find(".input-heading").addClass("_active");
			}
		});
	}

	$(document).on("change", ".input", function(){
		let $th = $(this);
		let parent = $(this).closest(".payment-form__label");
		let inputHeading = parent.find(".input-heading");
		let val = $th.val();

		if (val.length !== 0) {
			inputHeading.addClass("_active");
		} else {
			inputHeading.removeClass("_active");
		}
	});
};
