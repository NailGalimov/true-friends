export default () => {
	const openEmergencyBtn = document.querySelectorAll(".js-open-warning-modal");
	const modalEmergency = document.querySelector(".modal-emergency");

	openEmergencyBtn.forEach((btn) => {
		btn.addEventListener("click", (e)=>{
			let card = btn.closest(".card");
			let title = card.querySelector(".card__legend").textContent;
			let text = card.querySelector(".card__info p").textContent;
			let summ = card.querySelector(".js-summ").textContent;
			let need = card.querySelector(".js-need").textContent;
			let days = card.querySelector(".js-days").textContent;
			let imgSrc = card.querySelector(".card__image").getAttribute("src");
			let perc = card.querySelector(".card__progress-line-inner").getAttribute("style");

			modalEmergency.querySelector(".js-card-title").textContent = title;
			modalEmergency.querySelector(".card__info p").textContent = text;
			modalEmergency.querySelector(".js-modal-summ").textContent = summ;
			modalEmergency.querySelector(".js-modal-need").textContent = need;
			modalEmergency.querySelector(".js-modal-days").textContent = days;
			modalEmergency.querySelector(".card__image").setAttribute("src", imgSrc);
			modalEmergency.querySelector(".card__progress-line-inner").setAttribute("style", perc);
		});
	});
};
