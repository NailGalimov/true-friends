export default () => {
	const navLineContainer = document.querySelectorAll('.nav-line-container');

	if (navLineContainer) {
		navLineContainer.forEach((navContainer) => {
			const navLine = navContainer.querySelector('.payment-form__radio-btn-nav-line');
			const navItem = navContainer.querySelectorAll('.payment-form__radio-btn');

			navLine.style.width = `${navItem[0].offsetWidth}px`;
			navItem[0].classList.add("_is-active");

			navItem.forEach(el => {
				el.addEventListener('click', (e) => {
					navLine.style.width = `${e.currentTarget.offsetWidth}px`;
					navLine.style.left = `${e.currentTarget.offsetLeft}px`;
					navItem.forEach((item) => {
						item.classList.remove("_is-active");
					});
					el.classList.add("_is-active")
				});
			});
		});
	}
};
