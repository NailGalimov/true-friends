export default () => {
	const cards = document.querySelectorAll(".js-emergency-card");

	if (!cards) return;

	cards.forEach((card)=>{
		let summ = card.querySelector(".js-summ").textContent.replace(/\s+/g, '').split('₽').join('');
		let need = card.querySelector(".js-need").textContent.replace(/\s+/g, '').split('₽').join('');
		summ = +summ;
		need = +need;
		let line = card.querySelector(".card__progress-line-inner");
		let percDiff =  Math.round(((need - summ) * 100 ) / need);

		line.style.width = `${100 - percDiff}%`;
	});

};
