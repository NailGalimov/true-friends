import { Fancybox, Carousel, Panzoom } from "@fancyapps/ui";

export default () => {
	Fancybox.bind('[data-fancybox="gallery"]', {
		Toolbar: {
			display: [

			]
		},
		Carousel: {
			Navigation: {
				prevTpl:
					'<svg class="icon icon-arrow-prev slider-button__icon"><use xlink:href="img/sprite.svg#arrow-prev"></use></svg>',
				nextTpl:
					'<svg class="icon icon-arrow-next slider-button__icon"><use xlink:href="img/sprite.svg#arrow-next"></use></svg>',
			},
		},
		Image: {
			zoom: false,
		},
	});
}
