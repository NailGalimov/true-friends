import initSlider from "./initSlider";

export default () => {
	const openPetBtn = document.querySelectorAll(".js-open-pet-modal");
	const modalPet = document.querySelector(".js-modal-pet");

	openPetBtn.forEach((btn) => {
		btn.addEventListener("click", (e)=>{
			let card = btn.closest(".card");
			let title = card.querySelector(".card__legend").textContent;
			let subtitle = card.querySelector(".card__sub-legend").textContent;
			let text = card.querySelector(".js-pet-text").innerHTML;
			let imgs = card.querySelector(".js-pet-imgs").innerHTML;

			modalPet.querySelector(".js-modal-pet-heading").textContent = title;
			modalPet.querySelector(".js-modal-pet-subheading").textContent = subtitle;
			modalPet.querySelector(".js-modal-pet-text").innerHTML = text;
			modalPet.querySelector(".js-modal-pet-slide-wrapper").innerHTML = "";
			modalPet.querySelector(".js-modal-pet-slide-wrapper").innerHTML = imgs;
			initSlider();
		});
	});
};
