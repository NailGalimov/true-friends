// js-tabs-container - на контейнер
// js-tab-content, data-tabs-content="<tab-value>" и gl__menu-tab-content - на подменяемый контент

export default () => {
  const tabsContainerElems = document.querySelectorAll('.js-tabs-container');

  if (!tabsContainerElems) return;

  tabsContainerElems.forEach(tabsContainer => {
    const btnTabs = tabsContainer.querySelectorAll('.js-tab');
    const contentElems = tabsContainer.querySelectorAll('.js-tab-content');

    btnTabs.forEach((tab, index) => {
      if (tab.hasAttribute('checked')) {
        contentElems[index].classList.add('mod-show');
      } else {
        contentElems[index].classList.remove('mod-show');
      }

      tab.onchange = () => {
        contentElems.forEach(contentElem => {
          if (contentElem.getAttribute('data-tabs-content') === tab.value) {
            contentElem.classList.add('mod-show');
          } else {
            contentElem.classList.remove('mod-show');
          }
        })
      }
    })
  })
}
